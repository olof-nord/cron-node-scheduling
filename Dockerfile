FROM node:14.16-slim

# Install cron
RUN apt-get update && apt-get install --no-install-recommends -y cron=3.0pl1-128+deb9u1\
    gettext-base=0.19.8.1-2+deb9u1\
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Prepare logging
RUN touch /var/log/cron.log

WORKDIR /usr/src/app

COPY package.json package-lock.json ./
RUN npm ci

# build
COPY ./ /usr/src/app
RUN npm run build

EXPOSE 3000

ENV TZ=Europe/Berlin
ENV TEST_VAR1=HELLO1
ENV TEST_VAR2=HELLO2

# setup cron
RUN envsubst < crontab.template > /etc/cron.d/crontab
RUN chmod 0644 /etc/cron.d/crontab && crontab /etc/cron.d/crontab

RUN envsubst < run-pipeline.template > /root/run-pipeline.sh
RUN chmod +x /root/run-pipeline.sh

CMD ["cron", "-f"]