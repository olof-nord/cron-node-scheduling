# cron-node-scheduling

Does cron add anything special, if executing JavaScript per a schedule?

## Run and build locally

```bash
docker-compose up --build
```

or step by step

```bash
docker-compose build
```

```bash
docker-compose up -d
```

## Enter the container

To open a shell inside the container, use the following.

```bash
docker-compose exec cron-node-scheduling bash
```