import md5 from "md5";

export const run = () => {
    console.log("Hash calculation verification");

    const input1 = "2021-04-15 00:00:00";
    const input2 = process.env.TEST_VAR2;
    const hashSum = md5(input1+input2);

    console.log(`Input 1 is set to "${input1}"`);
    console.log(`Input 2 is set to "${input2}"`);
    console.log("MD5 hash sum is calculated to", hashSum);
}

run();
